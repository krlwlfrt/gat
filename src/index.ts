import got from 'got';

/**
 * Severity of a GitLab Alert
 */
export type GitLabAlertSeverity = 'critical'
  | 'high'
  | 'info'
  | 'low'
  | 'medium'
  | 'unknown';

/**
 * A GitLab alert
 * @see https://docs.gitlab.com/ee/operations/incident_management/integrations.html#customize-the-alert-payload-outside-of-gitlab
 */
export interface GitLabAlert {
  /**
   *  The title of the incident.
   */
  title?: string;

  /**
   * A high-level summary of the problem.
   */
  description?: string;

  /**
   * The time of the incident. If none is provided, a timestamp of the issue is used.
   * DateTime
   */
  start_time?: string;

  /**
   * For existing alerts only. When provided, the alert is resolved and the associated incident is closed.
   * DateTime
   */
  end_time?: string;

  /**
   * The affected service.
   */
  service?: string;

  /**
   * The name of the associated monitoring tool.
   */
  monitoring_tool?: string;

  /**
   * One or more hosts, as to where this incident occurred.
   */
  hosts?: string | string[];

  /**
   * The severity of the alert. Case-insensitive.
   * Can be one of: critical, high, medium, low, info, unknown.
   * Defaults to critical if missing or value is not in this list.
   */
  severity?: GitLabAlertSeverity;

  /**
   * The unique identifier of the alert. This can be used to group occurrences of the same alert.
   */
  fingerprint?: string | string[]

  /**
   * The name of the associated GitLab environment. Required to display alerts on a dashboard.
   */
  gitlab_environment_name?: string;
}

/**
 * Trigger a GitLab alert
 *
 * @param url URL of HTTP alert endpoint
 * @param token Authorization token for HTTP alert endpoint
 * @param gitLabAlert GitLab alert to trigger
 *
 * @see https://docs.gitlab.com/ee/operations/incident_management/alerts.html
 */
export async function triggerGitLabAlert(url: string, token: string, gitLabAlert: GitLabAlert): Promise<void> {
  await got.post(url, {
    json: gitLabAlert,
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
}

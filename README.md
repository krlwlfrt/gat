# @krlwlfrt/gat

[![pipeline status](https://img.shields.io/gitlab/pipeline/krlwlfrt/gat.svg?style=flat-square)](https://gitlab.com/krlwlfrt/gat/commits/master)
[![npm](https://img.shields.io/npm/v/@krlwlfrt/gat.svg?style=flat-square)](https://npmjs.com/package/@krlwlfrt/gat)
[![license)](https://img.shields.io/npm/l/@krlwlfrt/gat.svg?style=flat-square)](https://opensource.org/licenses/GPL-3.0)
[![documentation](https://img.shields.io/badge/documentation-online-blue.svg?style=flat-square)](https://krlwlfrt.gitlab.io/gat)

<!--
[![coverage](https://img.shields.io/gitlab/coverage/krlwlfrt/gat/master?style=flat-square)](https://gitlab.com/krlwlfrt/gat/-/pipelines)
-->

`@krlwlfrt/gat` is a small library to
trigger [GitLab alerts](https://docs.gitlab.com/ee/operations/incident_management/alerts.html).

Use `triggerGitLabAlert` and pass the url of the HTTP endpoint, the authorization token of the HTTP endpoint and the
data for the alert.

## Documentation

See [online documentation](https://krlwlfrt.gitlab.io/gat) for detailed API description.
